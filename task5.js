function Fun5(inputString, maxLength) {
	if (inputString.length <= maxLength) {
			return inputString; // Повертаємо вхідний рядок без змін, якщо він не перевищує максимальну довжину
	} else {
			const str = inputString.slice(0, maxLength) + "..."; // Відкидаємо зайві символи і додаємо трикрапку
			return str;
	}
}

const truncated = Fun5('Hello world!', 5);
console.log(truncated); 
