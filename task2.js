function capitalizeFirstLetter(inputString) {
	const firstLetter = inputString.charAt(0).toUpperCase(); // відділення першої літери та перетворення у верхній регістр
	const restOfString = inputString.slice(1); // слово без першої літери
	return firstLetter + restOfString; // з'єднання першої літери та решта слова
}

const convert = 'javascript';
const res = capitalizeFirstLetter(convert);
console.log(res);